# Catalog Model
class Catalog < ActiveRecord::Base
  include ElasticSearchable
  include ActivityHistory
  mount_uploader :image, ClientUploader

  def self.query(query)
    { query: { multi_match: {
      query: query,
      fields: [:name, :image],
      operator: :and,
      lenient: true }
    }, sort: { id: 'desc' }, size: count }
  end

  # Build index elasticsearch
  def as_indexed_json(_options = {})
    as_json(
      only: [:id, :name]
    )
  end
end
