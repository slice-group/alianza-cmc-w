module Admin
  # CatalogsController
  class CatalogsController < AdminController
    before_action :set_catalog, only: [:show, :edit, :update, :destroy]
    before_action :show_history, only: [:index]

    # GET /catalogs
    def index
      catalogs = Catalog.searching(@query).all
      @objects = catalogs.page(@current_page)
      @total = catalogs.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to catalogs_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    # GET /catalogs/1
    def show
    end

    # GET /catalogs/new
    def new
      @catalog = Catalog.new
    end

    # GET /catalogs/1/edit
    def edit
    end

    # POST /catalogs
    def create
      @catalog = Catalog.new(catalog_params)

      if @catalog.save
        redirect(@catalog, params)
      else
        render :new
      end
    end

    # PATCH/PUT /catalogs/1
    def update
      if @catalog.update(catalog_params)
        redirect(@catalog, params)
      else
        render :edit
      end
    end

    # DELETE /catalogs/1
    def destroy
      @catalog.destroy
      redirect_to admin_catalogs_path, notice: actions_messages(@catalog)
    end

    def destroy_multiple
      Catalog.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_catalogs_path(page: @current_page, search: @query),
        notice: actions_messages(Catalog.new)
      )
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_catalog
      @catalog = Catalog.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def catalog_params
      params.require(:catalog).permit(:name, :image)
    end

    def show_history
      get_history(Catalog)
    end
  end
end
