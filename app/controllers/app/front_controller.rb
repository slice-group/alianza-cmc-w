module App
  # FrontController
  class FrontController < AppController
    def index
      @catalogs = Catalog.all.each_slice(4).to_a
      @sliders = Catalog.all
      @services = Service.all
      @posts = KepplerBlog::Post.last 3
      @message = KepplerContactUs::Message.new
    end
  end
end
