module KepplerContactUs
  class ContactMailer  < ActionMailer::Base
  	def contact(message)
	    @message = message
	    mail(from: KepplerContactUs.mailer_from, to: KepplerContactUs.mailer_to, subject: @message.subject)
	end
  end
end
